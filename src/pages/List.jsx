import React from 'react'
import styled from 'styled-components'
import { hot } from 'react-hot-loader'
import { withRouteData, Link } from 'react-static'

const Listing = styled.li`
	list-style: none;
	time {
		color: #666;
		padding-right: 10px;
	}
`;


const List = withRouteData(({ entries, base_url }) => {

	entries.forEach(entry => console.log(entry.url));	
	const all = entries.map((entry, n) => (
			<Listing key={n}>
				<time>{entry.date}</time>
				<Link to={`${base_url}/${entry.url}`}>
					{entry.title}
				</Link>
			</Listing>
		)
	);

	return (
		<div>
			All Entries:
			<ul>
				{all}
			</ul>
		</div>
	)
});

export default hot(module)(withRouteData(List));