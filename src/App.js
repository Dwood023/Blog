import React from 'react'
import { Router } from 'react-static'
import styled, { injectGlobal } from 'styled-components'
import { hot } from 'react-hot-loader'
import Routes from 'react-static-routes'
import Nav from "./components/Nav"

import {text_color, bg_color} from "./constants";

injectGlobal`
body {
	@import url('https://fonts.googleapis.com/css?family=IBM+Plex+Mono:200,300,500');
	@import url("https://use.fontawesome.com/releases/v5.2.0/css/all.css");
	font-weight: 300;
	font-family: "IBM Plex Mono";
	font-size: 13px;
	margin: auto;
	
	background-color: ${bg_color};
	color: ${text_color};
	max-width: 700px; /* For center-column layout */
}
a {
	text-decoration: underline;
	color: ${text_color};
}
.content {
	padding: 1.5rem;
}
`

import {Head} from "react-static";

const App = () => (
	<Router>
	<Head>
	<script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>
	</Head>
	<div>
	<Nav />
	<div className="content">
	<Routes />
	</div>
	</div>
	<script>
	if (window.netlifyIdentity) {
		window.netlifyIdentity.on("init", user => {
			if (!user) {
				window.netlifyIdentity.on("login", () => {
					document.location.href = "/admin/";
				});
			}
		})
	}
	</script>
	</Router>
	)
	
	export default hot(module)(App)
	