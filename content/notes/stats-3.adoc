= PY1PR: 3 - Populations and Samples
David Wood <david.wood@student.reading.ac.uk>
16 Oct 2018
:stem: latexmath

== Lecture

Population:: All individuals within a defined group

Sample:: Subset of a population

Parameter:: Statistical observation of a whole population

History Effects:: Fallacy that previous outcomes affect independent future outcomes, eg. Getting heads twice makes next flip more likely to be tails

stem:[ x^2 + \pi ]

stem:[ x^2 ]

=== Samples

Samples are ideally random.

In practice, most samples are *opportunity samples*, as there is a bias in how individuals have the opportunity to be selected. eg. Seeing and responding to an advert

If samples are limited to a particular subset of a population, results cannot reliably be generalised to the superset, or population overall.

=== Normal Distribution

[sidenote]#Also known as Z-Distribution or Gaussian Distribution#

Most parameters of a population are normally distributed in a "bell-curve".

Z-score:: Deviation of a data-point from mean, assuming normal distribution

(x - mean)^2 / (n-1)

=== Sampling Distributions

If you want to generalise observations of a sample to a more general population, take many samples, then take the mean of the means of all samples.

The frequency histogram of these means is the sampling distribution.
If enough samples are included, the mean of the sampling distribution should predict the population mean.

Point Estimate:: A sample which has a different mean to other samples of the same population.

The smaller the sample, the further the mean will be from that of the population, generally.

Interval Estimate:: Aggregate of many point estimates into a guess at the true mean value of a population
