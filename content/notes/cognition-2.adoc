= PY1CG: 2 - Associative Learning
David Wood <david.wood@student.reading.ac.uk>
7 Oct 2018

Lecturer:: Dr Ciara McCabe

== Preface

Something about blackboard online discussion??
I wasn't listening

=== Objectives

- What is associative learning
- Kinds of association
* Pavlovian/Classical Conditioning
** Animal and humans
** Brain responses
* Instrumental/Operant Conditioning
- Selectivity (evidence of evolutionary adaptation)

== Lecture

This is a huge mess, clean up pls.

=== Learning as association

We've been generally talking about *learning*, but what is learning anyway?

==== Learning of "knowledge"

People learn *facts*, which is knowledge about the world in some abstract, cognitive sense.
To learn a fact, it has to be *understood* on some level - by relating it to concepts you already know.
[sidenote]#Is it possible to "know" something which has no other associated information? I can't even imagine #

When you think about it, all knowledge is predicated on existing related information.
A simplified example:

I learned a while ago that:

"Chickens are omnivores"footnote:[https://www.youtube.com/watch?v=bBFXzyp3sks]

To learn this fact, I'd need a whole lot of related information:

- What's a chicken? -> Species of animal
  * What's a species? -> Animals which can beget fertile offspring
    ** What's "fertile"
    ** What's "offspring"?
  * What's an animal?
    ** A living thing with x characteristics
- What's "omnivore"? -> Animal that eats meat and vegetation
  * What does "eating" mean?
  * What's meat?
  * What's vegetation?

This is a stupid and simplified example, but goes to demonstrate the necessity of *associated knowledge*.


==== Muscle Memory

People also learn *motor skills* - just knowing how to move your muscles a certain way.

. "Today I learned a fact about penguins: that they like fish"
+
This isn't pertaining to *behaviour*, but just associates an existing concept (an animal called a penguin) to a new property
. Today I learned a

Non-Associative learning:: Learning something about an event which occurs in isolation

[sidenote]#In isolation?? Is it possible to learn anything "in isolation"??#

Associative learning:: Learning *predictive relationships* between stimuli

=== Classical conditioning

Classical conditioning associates a "neutral" stimulus with one that has existing physiological significance (unconditioned (unconditioned).

Neutral Stimulus (NS):: Stimulus which induces no significant response
Conditioned Stimulus (CS):: Previously NS which is now associated with a conditioned response.
Unconditioned Stimulus (US):: Stimulus which induces natural (or preconditioned) physiological response.

What is "unconditioned"? Which responses are learned and which are we born with?

S-R learning:: Original theory - response is to CS, not
S-S learning:: New theory - CS -> US -> CR

==== Examples

===== Rabbit conditioning

Assoc. shock or air puff (US) with light or tone (NS).

===== Rat conditioning

Assoc shock with light, rat stops pressing lever for food when light is paired with lever.

Although the lever was not associated with a shock directly, the light (CS) produced a conditioned fear response by a new association with the lever.

===== Pigeon conditioning

Light comes on randomly, pigeon naturally peck at light.
Food is dispensed with light, whether pigeon pecks or not.
Still, pigeon pecks as though food were contigent on pecking.

Autoshaping:: CR is induced even though there is no

===== Taste aversion

Flavours are assoc. with chemically induced illness.
Taste aversion seems to last a long time, although power of conditioning depends strongly on delay between taste and sickness.

This is quite interesting specific case, taste aversion is surely a very fundamental behaviour to avoid poisoning in the wild.

Chemo patients get this bad - anything they taste during treatment may be associated to horrible chemo sickness.

==== Brain

Amygdala:: Almond shaped part of brain. "Phylogenetically" very old.

Once thought to govern fear responses. Now associated with learning in general.

Prefrontal cortex (PFC) seems to govern the activation of the amygdala, and control learning response.

==== Fear conditioning in humans

Insula and Amygdala show brain activity during fear response conditioning.

=== Operant Conditioning

About intentional behaviours.
Decisions made based on predicted pros/cons in outcome.

Operant conditioning associates positive or negative outcomes with behaviours/stimuli.

Difference is: Operant conditioning involves the conscious actions of the subject.

Discrimination:: Conditioning based on condition that specific stimuli are present, in addition to primary stimulus.

Salivation is innate response, but other examples are not innate: lever-pressing is not innate rat-behaviour.

=== Conditioning bias

Different NS are more/less easily conditioned into CS.
Individuals have existing readiness to learn some associations over others.
cHECK GARCIA for example.

Sickness is more readily associated with taste.
Shock is more readily associated with audio-visual,

What is the significance of these categories?
internal/external?

This seems to be "hardwired" biologically - learning works better through pre-existing "channels".
Animals are "prepared" to be poisoned by tastes, and physically injured by audio/visuals.

Makes sense right? These are the conditions that occur usually in the wild.

This is really interesting though, it suggests that the pre-disposition for certain LEARNINGS is ALREADY LEARNED, by our genetic anscestors.
Some "genetic learning" must have occured which makes us more prepared for some behaviours/outcomes.

This is "innate" nature - behaviours
