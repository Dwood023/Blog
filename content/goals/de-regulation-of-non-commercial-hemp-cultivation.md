---
title: De-Regulation of Non-Commercial Hemp Cultivation
description: >-
  The UK has recently legalised CBD as a food-supplement, and is enjoying a wave
  of popularity for its anxiolytic properties, along with other reported health
  benefits.


  Despite this step forward, the cultivation of hemp (or low-THC cannabis)
  remains restricted to license owners, which applies even to small scale
  growing for personal use.


  Among the requirements is a licensing fee exceeding £500 - clearly excessive
  for individuals only meaning to grow CBD for themselves in their own gardens.


  The home-office should recognise the hypocrisy of admitting the medical
  benefits and minimal harms of low-THC cannabis, yet still maintaining
  draconian regulation of simple home-gardening of this incredibly useful and
  increasingly popular plant.


  Cannabis strains below 0.2% THC content should be legal to grow for personal
  use, below a certain volume and without commercial profit.
---

