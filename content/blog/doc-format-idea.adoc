= Idea for Plaintext Document Format
David Wood <dwood023@live.co.uk>
19 Oct 2018



- Easily tag inline/block content with any class/id for custom CSS formatting
- User-defined "insertion rules"
+
Like setting "content" with "before/after" selectors in css, but there may be cases where css selectors aren't smart enough to infer where exactly the content should go.
More importantly, the content set in CSS cannot be dynamic (unless inline css in react?), better to set rules somewehere else? how will these rules/contents be defined if not in CSS (which people already know)


Basically, write easy plaintext syntax -> get conveniently tagged HTML -> write your own CSS rules
