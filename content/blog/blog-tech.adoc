= 1337 Blog Tech: Review
David Wood <Dwood023@live.co.uk>
24 Aug 2018

In this post, I'll describe how the pages of this site are generated and formatted.

I've been fiddling with this for a couple of weeks now, I was initially really happy with the results but some limitations are now materialising.

== Hosting and Routing

The website itself is written in React, compiled into static pages by React Static - a static site generator.footnote:[I usually use another static site generator called Gatsby, but I got fed up with all the complicated plugin/graphql nonsense. Getting plugins to work always takes me longer than just hacking stuff in myself. GraphQL is a bit unintuitive at first, but I like it now.] As far as I can tell, static site generators are a nice high-level wrapper for a web-bundler like webpack.

The result is a server-rendered site which can be hosted as-is. No backend of any kind.
For this reason, static sites can be easily hosted for free on platforms such as https://surge.sh[Surge.sh] or https://netlify.com[Netlify.com]footnote:[Netlify is even hosting this site right now, for free! Actually amazing service, I almost feel bad not paying them anything. Almost.].

== Posts

The blog and notes sections of this website are populated by documents written in https://asciidoctor.org[Asciidoc].

Asciidoc is a plaintext markup language like Markdown, but with many more useful features.

=== Writing in plaintext

Plaintext files have no hidden metadata, just text. Any metadata (styling, hyperlinks) is written _in-line_.

This is as opposed to encoded formats like those used by Microsoft Word for example. Non-plaintext formats hide information pertaining to the formatting and design of the document, and can only be styled through use of various buttons/hotkeys in a format-specific writing application.

Asciidoc files can be written in any text-editor, easily read even before being compiled to any output format and contain arbitrary formatting information, which can be fulfilled by your own CSS once converted to HTML.
